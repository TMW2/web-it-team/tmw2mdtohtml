#!/bin/bash
# (C) Copyright, Livio Recchia 2020
# File under same license

echo $0

# Packages needed:
# pandoc

# --- Script purpose constants values
# GIT Repo setting
wikiGitURL="https://git.themanaworld.org/ml/docs.wiki.git"
wikiGitDirName="docs.wiki"

# Set this variable to an empty string for verbose output
quietCall="--quiet"

# --- Output status routines
terminalEscapeCleanRow="\033[2K"

# Makes script exit if previous command returned an error (status not zero)
exitOnFailure(){
    if [ $? -gt 0 ]; then
        echo -e "\033[31;1mProcessing aborted.\033[0m"
        exit 255
    elif [[ $? == "127" ]]; then
        echo -e "\033[31;1mScript failure: maybe a system package is missing.\033[0m"
    fi
}

# Defined status if not initialized elsewhere
actionStatus="DONE"
# Prints coloured status messages on output
printAction(){
    if [[ $actionStatus = "SKIP" ]]; then
        echo -en "[$0][\033[33;1mSKIP\033[0m] $action"
    elif [[ $actionStatus = "WAIT" ]]; then
        echo -en "[$0][\033[33;1mWAIT\033[0m] $action\r"
    elif [[ $actionStatus = "DONE" ]]; then
        echo -e "$terminalEscapeCleanRow[$0][\033[32;1mDONE\033[0m] $action"
    elif [[ $actionStatus = "FAIL" ]]; then
        echo -e "$terminalEscapeCleanRow[$0][\033[31;1mFAIL\033[0m] $action"
    else
        echo -en "$terminalEscapeCleanRow[$0][\033[33;1m$actionStatus\033[0m] $action\r"
    fi
}

# Launch command and prints a generalized string on its status. Useful for quite or critical commands.
launch(){
    # Action in progress
    actionStatus="WAIT"
    printAction
    $($command)
    if [[ $? == "0" ]]
    then
        actionStatus="DONE"
    else
        actionStatus="FAIL"
    fi
    printAction
}

# --- Script routines

updateMarkdownFiles(){
    # Delete old directory
    action="Clearing";
    command="rm $wikiGitDirName -Rf"; launch;

    # Download TMW2 wiki markdown files
    action="Dowloading wiki"
    command="git clone $wikiGitURL $quietCall"; launch; exitOnFailure
}

# --- Main starts here!

updateMarkdownFiles
cd $wikiGitDirName

# Put together html files (note there's no extension on files)
# Note: ${markdownFile:0:-3} means excluding last three characters
processedFiles=1
# Counting file number to calculate process
lines=$(find . -name '*.md' | wc -l )
for markdownFile in $(find . -name '*.md')
do
    echo '<HTML>' > ${markdownFile:0:-3}
    echo '<HEAD>' >> ${markdownFile:0:-3}
    echo '<meta charset="utf-8" /><meta http-equiv="X-UA-Compatible" content="IE=edge"><meta http-equiv="content-type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width, initial-scale=1"><meta name="description" content="TMW2: Moubootaur Legends is an open source MMORPG project, using the ManaPlus client. In this game, the player is given the opportunity to permanently change the world and its story. The players are free to decide how they want the world to develop, and their actions change the storyline, besides always being able to join the Development Team."><meta name="keywords" content="MMORPG, RPG, TMW2, MMO, Open Source, Saulc, fantasy, free, game, manaplus, online, combat, free software, free RPG, mouboo, moubootaur">' >> ${markdownFile:0:-3}
    echo '<link href="https://moubootaurlegends.org/static/normalize.css" rel="stylesheet"><link href="https://moubootaurlegends.org/static/site.css" rel="stylesheet">' >> ${markdownFile:0:-3}
    echo '<title>Moubootaur Legends Wiki</title>' >> ${markdownFile:0:-3}
    echo '</HEAD>' >> ${markdownFile:0:-3}
    echo '<BODY>' >> ${markdownFile:0:-3}
    echo '<header><a class="header-logo" href="/"><img src="https://moubootaurlegends.org/static/logo.png"></a><div class="title-menue"><h1>Moubootaur Legends Wiki</h1></div></header><div class="content"><div class="site-header"></div><div class="center-section">' >> ${markdownFile:0:-3}
    #~ markdown --html4tags $markdownFile >> ${markdownFile:0:-3}
    # Note: older pandoc version does not support --quiet
    #~ pandoc $quietCall -t html4 $markdownFile >> ${markdownFile:0:-3}; exitOnFailure
    pandoc -t html4 $markdownFile >> ${markdownFile:0:-3}; exitOnFailure
    # Creating a text-only wiki (content is not sorted)
    #~ pandoc $quietCall -t plain $markdownFile >> plain_wiki.txt; exitOnFailure
    pandoc -t plain $markdownFile >> plain_wiki.txt; exitOnFailure
    echo '</div></div><footer><div class="footer-content"><div class="footer-part" style="text-align: right;"><div class="footer-community-links"><a href="https://www.youtube.com/channel/UCp-aVxXfcQWFvQde6kGvYcg" class="footer-communitybutton"><i class="fab fa-youtube fa-lg" aria-label="youtube"></i></a><a href="https://discord.gg/J4gcaqM" class="footer-communitybutton" rel="nofollow"><i class="fab fa-discord fa-lg" aria-label="discord"></i></a><a href="https://git.themanaworld.org/ml" class="footer-communitybutton"><i class="fab fa-gitlab fa-lg" aria-label="gitlab"></i></a><a href="https://www.patreon.com/TMW2" class="footer-communitybutton"><i class="fab fa-patreon fa-lg" aria-label="patreon"></i></a><a href="https://www.indiedb.com/games/tmw2-mouboutaur-legends" title="View TMW2: Moubootaur Legends on Indie DB" target="_blank"><img src="https://media.indiedb.com/images/global/indiedb.png" style="width:16px;height:16px" alt="IndieDB" /></a></div><span class="footer-copyright">Copyright © 2018 TMW-2 Team</span></div></div></footer>' >> ${markdownFile:0-3}
    echo "</BODY>" >> ${markdownFile:0:-3}
    echo "</HTML>" >> ${markdownFile:0:-3}

    #~ # Deleting markdown file from directory
    rm $markdownFile
    exitOnFailure
    
    # Updating process calculation
    processedFiles=`expr $processedFiles + 1`;
    action=$(printf "Processing %u of %u file... $markdownFile\r" $processedFiles $lines)
    actionStatus=$(printf "%03u%%" $(expr $processedFiles '*' 100 / $lines))
    printAction
    
done
cp home index.html
action="Markdown processed"; actionStatus="DONE"; printAction

#~ action="Creating PDF";
#~ command="pandoc --pdf-engine=xelatex --toc _mdmerge -o wiki.pdf"; launch;

###

# Misc indexes
find . -type d -exec touch '{}/index.html' \;

# Deploy
cd ..

# [Livio]: That directory will be empty on my system: I don't know why has to be created.
# [Jesusalva]: I added it to .gitlab-ci.yml D: It used to hold results
#mkdir -p public

action="Clearing .git data from markdown sources";
command="rm -Rf $wikiGitDirName/.git"; launch; exitOnFailure

action="Moving html pages to public directory";
command="mv $wikiGitDirName/* public/"; launch; exitOnFailure
action="Wiki Ported"; actionStatus="DONE"; printAction

# --- NOTES

# Test code for replace text inside markdown files. Not actually needed.
#~ sed -i 's/old-text/new-text/g' _sum.md
#~ cat *.md > _sum.md
#~ sed -i 's/](/](#/g' _sum.md
#~ sed -i 's/](#http/](http/g' _sum.md
#~ markdown _sum.md > output.html

# Find command example:
# find . -name '*.md' -or -name '*.nd'
